# Alpine Go

These are a set of packages that help with interacting with various Alpine Linux
related systems. These things include:

* APKBUILD files
* APKINDEX files
* .apk files
* release.json

## Packages

In-depth documentation for this package can be found on [pkg.go.dev][go-dev]

### apkbuild

```go
import "gitlab.alpinelinux.org/alpine/go/apkbuild"
```

Package apkbuild parses APKBUILD files and returns the metadata.

### repository

```go
import "gitlab.alpinelinux.org/alpine/go/repository"
```

Package repository parses .apk and APKINDEX files, providing information about
all packages in the index.

### releases

```go
import "gitlab.alpinelinux.org/alpine/go/releases"
```

Package releases provides functions and structures to get information about
Alpine Linux releases.

```go
rels, err := releases.Fetch()

for _, releaseBranch := range rels.ReleaseBranches {
    fmt.Printf("Release %s eol: %s\n", releaseBranch.RelBranch, releaseBranch.EolDate)
}
```

### version

``` go
import "gitlab.alpinelinux.org/alpine/go/version"
```

Package version tokenizes a apk package version using the same algorithm as
apk-tools uses.

## Community projects using alpine/go

| Project                                                              | Description                                                       |
|----------------------------------------------------------------------|-------------------------------------------------------------------|
| [apkcirkledep](https://gitlab.alpinelinux.org/ptrcnull/apkcircledep) | Report circular dependencies between packages in aports           |
| [apkgquery](https://gitlab.alpinelinux.org/kdaudt/apkgquery)         | Query aports using an expression language                         |

## License

MIT

[go-dev]:https://pkg.go.dev/gitlab.alpinelinux.org/alpine/go
